﻿import java.util.Scanner;

class Fahrkartenautomat
{
	//TODO String resource file, Complete list
	public static final String start_text_ger = "Fahrkartenbestellvorgang:\n=========================\n\n";
	public static final String ticketlist_ger = "Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\nEinzelfahrschein Regeltarif AB [2,90 EUR] (1)\nTageskarte Regeltarif AB [8,60 EUR] (2)\nKleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n";
	public static final String invalid_count_ger = "\nUngültige Ticketanzahl. \nGültige anzahl 1-10stk. \nWir haben 1 Ticket berechnet.\n";
	public static final String transaction_complete_ger = "\nVergessen Sie nicht, den Fahrschein\nvor Fahrtantritt entwerten zu lassen!\nWir wünschen Ihnen eine gute Fahrt.";
	//Possible coin sizes, goes left to right. not high to low
	public static int[] muenzgroessen = {200,100,50,20,10,5};
	//Price list for tickets 1-3[Dynamic Increase]. Text for ticket list not dynamic!
	public static int[] price = {290,860,2350};//In Cents
    
	public static void main(String[] args)
    { 	  	
    	boolean error = false;
    	while (!error) {
	    	try {//Bestellung->Bezahlen(Preis)->Rückgeld(Rückgeld)
	    		rueckgeldAusgeben(fahrkartenBezahlen((int)fahrkartenbestellungErfassen()));
	    		System.out.print("\n\n\n\n");}//Empty Lines for new Order
	    	catch (java.util.InputMismatchException IME) {
	    		pln("Ungültige Angabe. Nur Zahlen erlaubt. Kein . statdessen bitte , Nutzen");warte(2200);} 
	    	catch (Exception e) {e.printStackTrace();error = true;pln("An Critical error occured. This System resarts now");warte(1200);error=false;}}
    }
    
    public static void warte(int millisekunden) {try {Thread.sleep(millisekunden);} catch (InterruptedException e) {e.printStackTrace();}}
    
    public static void pln(String Str) {System.out.println(Str);}//Print line in shorter name (Alias), TODO look at java doc for delegates
    
    public static void muenzeAusgeben(int betrag, String einheit) {pln(betrag + " " + einheit);} //Print Fromat : [Text-Val] [Text-Euro/Cent]
    
    public static double fahrkartenbestellungErfassen() {
    	@SuppressWarnings("resource")
		Scanner tastatur = new Scanner(System.in);
       int anzTickets;
       int zuZahlenderBetrag = 0;      
       int tickettype = 0;
       
       pln(start_text_ger+ticketlist_ger);
       //Loop until valid tickettype
       	 //Use corresponding ticketprice for calculations
           //Exit loop when valid tickettype
       while (true){
          pln("Ihre Wahl:" + (int)(Math.round(tastatur.nextDouble())));
          if (tickettype >= 1 && tickettype <=3){zuZahlenderBetrag = price[tickettype-1];break;}
          else {pln(">>falsche Eingabe<<");}}       
       
       System.out.print("Anzahl der Tickets: ");
       anzTickets = tastatur.nextInt();       
       if ((anzTickets < 1) || (anzTickets > 10)) {anzTickets = 1; pln(invalid_count_ger);};
    	   
       zuZahlenderBetrag = (zuZahlenderBetrag * anzTickets); //*100
       return (double)zuZahlenderBetrag;
    }
       
     public static int fahrkartenBezahlen(int zuZahlenderBetrag) {
       @SuppressWarnings("resource")
       Scanner tastatur = new Scanner(System.in);
       int eingezahlterGesamtbetrag = 0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag){
    	   System.out.printf("Noch zu zahlen: %.2f EURO \n", (double)((double)(zuZahlenderBetrag - eingezahlterGesamtbetrag) / 100));
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingezahlterGesamtbetrag += (int)(Math.round(tastatur.nextDouble() * 100));}
       fahrkartenAusgeben();
       return (eingezahlterGesamtbetrag - zuZahlenderBetrag);
    }
	
	public static void fahrkartenAusgeben() {       
       pln("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++){System.out.print("=");warte(250);}
       pln("\n\n");
    }
	
	public static void rueckgeldAusgeben(int rückgabebetragCent) {

		if(rückgabebetragCent > 0){
    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO \n", (double)((double)rückgabebetragCent / 100));
    	   pln("wird in folgenden Münzen ausgezahlt:");    	      	   
    	   
    	   //While change money >= last coin
    	     //While change money>= current coin 
    	       //if coin val is over or equal 100cents then convert to euro. if else keep as cents
    	   int cmg = 0; 
    	   while (rückgabebetragCent >= muenzgroessen[muenzgroessen.length-1]) {    		   
    		   while(rückgabebetragCent >=  muenzgroessen[cmg]){    			  
            	  if  (muenzgroessen[cmg] >= 100) {muenzeAusgeben((muenzgroessen[cmg] / 100), "EURO");}
            	  else {muenzeAusgeben(muenzgroessen[cmg], "CENT");}
            	  rückgabebetragCent -= muenzgroessen[cmg];}cmg +=1;}
		}
       pln(transaction_complete_ger);warte(1400);
    }
}