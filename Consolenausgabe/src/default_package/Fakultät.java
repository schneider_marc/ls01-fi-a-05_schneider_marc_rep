package default_package;

public class Fakult�t {

	public static void main(String[] args) {
		
		fp("0!", "", "1");
		fp("1!", "1", "1");
		fp("2!", "1 * 2", "2");
		fp("3!", "1 * 2 * 3", "6");
		fp("4!", "1 * 2 * 3 * 4", "24");
		fp("5!", "1 * 2 * 3 * 4 * 5", "120");
		    
	}
	
	public static void fp(String s1, String s2, String s3) {
		System.out.printf( "%-5s = %-19s = %4s\n" ,   s1, s2, s3);
	}

}