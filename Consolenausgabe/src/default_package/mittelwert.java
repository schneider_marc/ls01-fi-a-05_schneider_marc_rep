package default_package;

public class mittelwert {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// (E) "Eingabe"
	    // Werte f�r x und y festlegen:
	    // ===========================
	    double x = 2.0;
	    double y = 4.0;
	    
	    // (A) Ausgabe
	    // Ergebnis auf der Konsole ausgeben:
	    // =================================
	    System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, berechneMittelwert(x,y));
	}

	public static double berechneMittelwert(double x, double y) {
		// (V) Verarbeitung
	    // Mittelwert von x und y berechnen: 
	    // ================================
	    return (x + y) / 2.0;		
	}
	
}
